package br.com.conductor.wsfabrica.test.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
	//NomeMetodo.asmx
})
public class AllTests {
	
}
