package br.com.conductor.wsfabrica.ws_exemplo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "req"
})
@XmlRootElement(name = "Exemplo")
public class Exemplo {
	protected ExemploReq req;
	
	public ExemploReq getReq() {
		return req;
	}
	
	public void setReq(ExemploReq value) {
		this.req = value;
	}
	
}
