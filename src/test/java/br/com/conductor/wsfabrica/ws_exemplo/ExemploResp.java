package br.com.conductor.wsfabrica.ws_exemplo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExemploResp", propOrder = {
    "codRetorno",
    "descricaoRetorno",
    "status"
})

public class ExemploResp {
	@XmlElement(name = "CodRetorno")
    protected int codRetorno;
    @XmlElement(name = "DescricaoRetorno")
    protected String descricaoRetorno;
    @XmlElement(name = "status")
    protected String status;
    
    
    public int getCodRetorno() {
        return codRetorno;
    }

    public void setCodRetorno(int value) {
        this.codRetorno = value;
    }
    
    public String getDescricaoRetorno() {
        return descricaoRetorno;
    }

    public void setDescricaoRetorno(String value) {
        this.descricaoRetorno = value;
    }
    
    public String getStatus() {
        return status;
    }

    public void setStatus(String value) {
        this.status = value;
    }
}
