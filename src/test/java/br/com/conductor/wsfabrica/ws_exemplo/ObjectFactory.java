
package br.com.conductor.wsfabrica.ws_exemplo;

import javax.xml.bind.annotation.XmlRegistry;



@XmlRegistry
public class ObjectFactory {


    public ObjectFactory() {
    }

    public ExemploResp createExemploResp() {
        return new ExemploResp();
    }

    public ExemploReq createExemploReq() {
        return new ExemploReq();
    }

  

}
