package br.com.conductor.wsfabrica.ws_exemplo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExemploReq", propOrder = {
    "param1",
    "param2",
    "param3"
})

public class ExemploReq {
	@XmlElement(name = "param1", required = true, nillable = true)
    protected String param1;
    @XmlElement(name = "param2", required = true, nillable = true)
    protected String param2;
    @XmlElement(name = "param3")
    protected int param3;
    
    
    public String getParam1() {
    	return param1;
    }
    
    public void setParam1(String value) {
    	this.param1 = value;
    }
    
    public String getParam2() {
    	return param2;
    }
    
    public void setParam2(String value) {
    	this.param2 = value;
    }
    
    public int getParam3() {
    	return param3;
    }
    
    public void setParam3(int value) {
    	this.param3 = value;
    }
    
}






