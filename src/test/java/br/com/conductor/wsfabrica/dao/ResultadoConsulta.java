package br.com.conductor.wsfabrica.dao;

import java.util.HashMap;

public class ResultadoConsulta {

	private HashMap<String, String> registros;
	private ResultadoConsulta next;

	public ResultadoConsulta() {
		this.registros = new HashMap<String, String>();
		this.next = null;
	}

	public String retornarRegistro(String retorno) {
		if (registros.containsKey(retorno))
			return registros.get(retorno);
		else
			return null;
	}

	public void inserirRegistros(String chave, String valor) {
		registros.put(chave, valor);
	}

	public ResultadoConsulta getNext(ResultadoConsulta resultadoConsulta) {
		return resultadoConsulta.next;
	}

	public void setNext(ResultadoConsulta resultadoConsulta) {
		resultadoConsulta.next = resultadoConsulta;
	}

	public HashMap<String, String> retornarRegistros(
			ResultadoConsulta resultadoConsulta) {
		return resultadoConsulta.registros;
	}

	public void inserirProximo(ResultadoConsulta resultadoConsulta,
			ResultadoConsulta inicial) {
		if (inicial == null)
			inicial = resultadoConsulta;
		else {
			ResultadoConsulta aux = inicial.next;
			while (aux != null) {
				inicial = aux;
				aux = aux.next;
			}
			inicial.next = resultadoConsulta;
		}

	}

	public int getSize(ResultadoConsulta resultadoConsulta) {
		int a = 0;
		while (resultadoConsulta != null) {
			if (resultadoConsulta.registros.size() != 0)
				a++;
			resultadoConsulta = resultadoConsulta.getNext(resultadoConsulta);
		}
		return a;
	}

}
