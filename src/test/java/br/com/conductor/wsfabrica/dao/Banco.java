package br.com.conductor.wsfabrica.dao;

import java.sql.Connection;

public class Banco {

	private static Connection conn = null;
	
	
	public static Connection getConn() {
		return conn;
	}

	public static void setConn(Connection conn) {
		Banco.conn = conn;
	}

}
