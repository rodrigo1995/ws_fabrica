package br.com.conductor.wsfabrica.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;

import br.com.conductor.wsfabrica.common.Property;
import br.com.conductor.wsfabrica.dao.ResultadoConsulta;
import br.com.conductor.wsfabrica.util.Log;
import br.com.conductor.wsfabrica.util.Utils;


public class DBUtils {
	private PreparedStatement ps;
	private LinkedList<String> listaParametros;
	private ResultSet rs;
	private ResultadoConsulta resultadoConsulta;
	private String db_emissor;
	private String ip_emissor;
	private boolean stopQuery;
	
	public DBUtils() {
		this.ps = null;
		this.listaParametros = new LinkedList<String>();
		this.rs = null;
		this.resultadoConsulta = null;
		this.db_emissor = Property.DB_INSTANCE;
		this.ip_emissor = Property.DB_IP;
		this.stopQuery = true;
		
	}
	
	public DBUtils(LinkedList<String> listaParametros) {
		this.ps = null;
		this.listaParametros = listaParametros;
		this.rs = null;
		this.resultadoConsulta = null;
		this.db_emissor = Property.DB_INSTANCE;
		this.ip_emissor = Property.DB_IP;
		this.stopQuery = true;
		
	} 
	
	public void configInstanceDB(String novaInstancia){
		// Guardando a instancia antiga. 
		Property.DB_INSTANCE = novaInstancia;
	}
	
	public void configIpDB(String novoIp){
		Property.DB_IP = novoIp;
	}
	
	public void isStopQuery(boolean stopQuery){
		this.stopQuery = stopQuery;
	}

	
	private void configPreparedStatement(String query) {
		try {
			ConfiguracaoBanco.conectarBanco();
			Log.info("Realizando instrucao SQL...");
			this.ps = Banco.getConn().prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		} catch (Exception e) {
			this.fecharConexao();
			this.reconfigurarBanco();
			Log.info("Erro ao tentar configurar a conex�o com o banco");
			Utils.wait(30000);
			Log.info("Tentando realizar conex�o novamente");
			configPreparedStatement(query);
		}
	}
	
	public void configListaParametros(LinkedList<String> listaParametros){
		this.listaParametros = listaParametros;
	}
	
	private ResultadoConsulta executarQuery(String query) {
		try {
			rs = ps.executeQuery();
			ResultSetMetaData rsm = rs.getMetaData();
			Log.info("Consulta terminada, retornando resultados.");
			while (rs.next()) {
				ResultadoConsulta novoRegistro = new ResultadoConsulta();
				for (int a = 1; a <= rsm.getColumnCount(); a++) {
					novoRegistro.inserirRegistros(rsm.getColumnName(a)
							.toUpperCase(), rs.getString(a));
					Log.info("Massa: "+rsm.getColumnName(a)+" de valor: "+rs.getString(a)+" obtida com sucesso!");
				}
				
				if (this.resultadoConsulta == null) {
					this.resultadoConsulta = novoRegistro;
				} else {
					this.resultadoConsulta.inserirProximo(novoRegistro,this.resultadoConsulta);
				}
			}

			if ((resultadoConsulta == null && stopQuery) || (stopQuery && resultadoConsulta.getSize(this.resultadoConsulta) == 0 )) {
				this.reconfigurarBanco();
				this.fecharConexao();
				Log.erro("Query n�o retornou nenhum registro v�lido");
				
			} else if(resultadoConsulta == null || resultadoConsulta.getSize(this.resultadoConsulta) == 0){
				Log.info("Consulta n�o retornou nehum registro");
			}
		} catch (SQLException e) {
			this.fecharConexao();
			e.printStackTrace();
			this.reconfigurarBanco();
			Log.erro("Erro ao tentar fazer a consulta ao banco de dados");
		} 
		return this.resultadoConsulta;
		
	}
	
	private int executarUpdate(String update){
		try{
		return ps.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
			this.fecharConexao();
			Log.erro("Erro ao fazer o update, insert ou delete.");
		}
		return -1;
	}
	
	private void configurarParametrosPreparedStatement(){
		try {
			if (this.listaParametros.size() > 0 && ps!=null) {
				Iterator<String> i = listaParametros.iterator();
				int contador = 1;
				while (i.hasNext()) {
					ps.setString(contador, i.next());
					contador++;
				}
			} else if(ps== null) {
				Log.erro("Erro ao tentar fazer a configura��o de par�metros. Verifique o PreparedStatement e sua configura��o");
			}
		} catch (SQLException e) {
			this.fecharConexao();
			e.printStackTrace();
			this.reconfigurarBanco();
			Log.erro("Erro ao tentar fazer a consulta em banco. Verifique os itens da sua lista de par�metros e tente novamente.");
		} 
	}
	
	private void fecharConexao(){

		try {
			if (rs != null) {
				rs.close();
			}
			if (ps != null) {
				ps.close();
			}
			if (Banco.getConn() != null) {
				Banco.getConn().close();
			}
		} catch (Exception e2) {
			Log.info("Erro ao tentar fechar conex�o ao banco");
		} finally{
			this.reconfigurarBanco();
		}
	}
	
	public ResultadoConsulta getConsulta(String query){
		this.resultadoConsulta=null;
		this.configPreparedStatement(query);
		this.configurarParametrosPreparedStatement();
		this.executarQuery(query);
		this.fecharConexao();
		//Inser��o: Limpando os par�metros para uma poss�vel pr�xima consulta. 
		this.listaParametros = new LinkedList<String>();
		return this.resultadoConsulta;
	}

	
	public void update(String update){
		this.resultadoConsulta=null;
		this.configPreparedStatement(update);
		this.configListaParametros(this.listaParametros);
		this.configurarParametrosPreparedStatement();
		this.executarUpdate(update);
		this.fecharConexao();
		this.listaParametros = new LinkedList<String>();
	}
	
	public void adicionarParametro(String parametro){
		this.listaParametros.add(parametro);
	}
	
	private void reconfigurarBanco(){
		Property.DB_INSTANCE = this.db_emissor;
		Property.DB_IP = this.ip_emissor;
	}
	
	

}
