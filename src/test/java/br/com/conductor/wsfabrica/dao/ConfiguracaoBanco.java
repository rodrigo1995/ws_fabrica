package br.com.conductor.wsfabrica.dao;

import java.sql.Connection;
import java.sql.DriverManager;

import br.com.conductor.wsfabrica.common.Property;

public class ConfiguracaoBanco {

	public static void conectarBanco() throws Exception {
		String tipoBanco = Property.DB_TIPO_BANCO;
		String hostBanco = ConfiguracaoBanco.getStringConexao();
		String usuarioBanco = Property.DB_USUARIO;
		String senhaBanco =  Property.DB_SENHA;

		if (tipoBanco.toUpperCase().equals("SQLSERVER")) {
			try {
				Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
				Connection conn = DriverManager.getConnection(hostBanco, usuarioBanco, senhaBanco);
				conn.setAutoCommit(true);
				Banco.setConn(conn);
			} catch (Exception e) {
				throw new Exception("N�o foi poss�vel conectar ao Banco ("
						+ tipoBanco + " )", e);
			}
		}
	}
	
	public static String getStringConexao()
			throws Exception {
		if (Property.DB_TIPO_BANCO.toUpperCase().equals("SQLSERVER")) {
			try {
				Class.forName("net.sourceforge.jtds.jdbc.Driver");
			} catch (Exception e) {
			}

				return getStringConexaoPadraoSQLServer();

		} else {
			throw new Exception("Tipo de Banco Invalido ("
					+Property.DB_TIPO_BANCO+ ")");
		}
	}

	private static String getStringConexaoPadraoSQLServer() {
		return "jdbc:jtds:sqlserver://" + Property.DB_IP+";DatabaseName="+Property.DB_INSTANCE+";domain="+Property.BD_DOMAIN+";trusted_connection=yes";
	}



}
