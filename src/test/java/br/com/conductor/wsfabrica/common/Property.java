package br.com.conductor.wsfabrica.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import br.com.conductor.wsfabrica.util.Log;
/**
* @author thiago.freire
* Define o caminho do driver dos diferentes browsers
* Acessa as configura��es definidas no config.properties e retorna o valeu do Propertie 
*/
public abstract class Property {

	public static String URL_WS;
	public static String DB_TIPO_BANCO;
	public static String DB_USUARIO;
	public static String DB_SENHA;
	public static String DB_IP;
	public static String DB_INSTANCE;
	public static String BD_DOMAIN;
	public static String BD_CONTROLE_ACESSO;
	public static String EMISSOR;
	public static String EXECUTAR_VIA_MAVEN;
	public static String BD_INSTANCE_FRAUDE;
	public static String BD_IP_FRAUDE;
	public static String ID_EMISSOR;
	public static String IP_DB_MONGO;
	public static String PORT_DB_MONGO;
	public static String BD_IP_CONTROLEACESSO;
	
	
	
	public static Properties propConfigTeste;
	public static Properties propConfigEmissor;
	private static final String PROP_FILE_TESTE = "config.properties";
	private static final String PROP_FILE_EMISSOR;
	static{
		
		/*
		 * Configura��es gerais: Arquivo config.properties. 
		 * 
		 * Utilizado para informar o emissor executando os testes e ip/porta do mongo. Nesta configura��o podem haver
		 * tr�s cen�rios: 
		 * 
		 * IP e porta vazios neste arquivo: Ser� primeiro considerado o arquivo de configura��o do emissor. Caso l� tamb�m esteja
		 * vazio, ser� considerado IP e porta padr�o. 
		 * 
		 * */
		EXECUTAR_VIA_MAVEN			= getConfig("executar.maven");
		EMISSOR						= getModoTeste(); 
		IP_DB_MONGO					= getConfig("banco.mongo.ip");
		PORT_DB_MONGO 				= getConfig("banco.mongo.porta");
		PROP_FILE_EMISSOR 			= "src/test/resources/emissores/".concat(EMISSOR).concat(".properties");
		
	}
	static {
		try {
			FileInputStream fileInputStream = null;
			File file = new File(PROP_FILE_EMISSOR);

			if (file.exists()) {
				propConfigEmissor = new Properties();
				fileInputStream = new FileInputStream(file);
				propConfigEmissor.load(fileInputStream);
				fileInputStream.close();
			}
		} catch (Exception e) {
			Log.excecao(e);
		}
	}
	/**
	 * Metodo para pegar o valor de alguma propriedade no arquivo de configuracao do Selenium
	 * O caminho e o nome do arquivo pode ser trocados
	 */
	private static String getConfig(String name) {
		Properties properties = new Properties();
		String value = null;
		try {
			properties.load(Property.class.getClassLoader()
					.getResourceAsStream(PROP_FILE_TESTE));
		    value = properties.getProperty(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return value;
	}
	private static String getModoTeste() {
		String retorno = null;
		try {
			if(Boolean.getBoolean(EXECUTAR_VIA_MAVEN)){
				retorno =  System.getProperty("conf");
			}else{
				retorno = getConfig("emissor");
			}
		} catch (Exception e) {
			Log.erro("Problema ao buscar o modo de execu��o. Verifique seu config.properties");
		}
		return retorno;
	}
	
	
	/**
	 * Metodo para pegar o valor de alguma propriedade no arquivo de configuracao do ambiente do CsLight
	 * O caminho e o nome do arquivo pode ser trocados
	 */
	public static String get(String name) {
		String value = null;
		try {
			if (propConfigEmissor != null) {
				value = propConfigEmissor.getProperty(name);
			}
		} catch (Exception e) {
			Log.excecao(e);
		}
		return value;
	}
	// M�todo criado para verificar se o property existe. Caso existe, retorne sempre o valor que est� 
	// vinculado a ele, se n�o retorne o valor original da String. 
	public static String get(String name,String originalValue) {
		String value = null;
		try {
			if (propConfigEmissor != null) {
				value = propConfigEmissor.getProperty(name);
			if(value == null || value.isEmpty()) return originalValue;
			}
		} catch (Exception e) {
			Log.excecao(e);
		}
		return value;
	}
	static {
		URL_WS 				= get("request.endpoint");
		DB_TIPO_BANCO 		= get("banco.tipo");
		DB_IP				= get("banco.ip");
		DB_INSTANCE			= get("banco.instance");
		BD_DOMAIN			= get("banco.domain");
		DB_USUARIO			= get("banco.user");
		DB_SENHA			= get("banco.senha");
		BD_CONTROLE_ACESSO	= get("banco.name.controle.acesso");
		BD_IP_FRAUDE		= get("banco.fraude.ip");
		BD_INSTANCE_FRAUDE	= get("banco.fraude.instance");
		ID_EMISSOR			= get("id.emissor");
		IP_DB_MONGO			= get("banco.mongo.ip",IP_DB_MONGO);
		PORT_DB_MONGO 		= get("banco.mongo.porta",PORT_DB_MONGO);
		BD_IP_CONTROLEACESSO= get("banco.ip.controleacesso");
		
		
	}
}
