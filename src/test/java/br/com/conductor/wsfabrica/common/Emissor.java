package br.com.conductor.wsfabrica.common;

public class Emissor {
	public static final String EO2 		= 	  "EO2";
	public static final String FORTBRASIL =   "FORTBRASIL";
	public static final String GABRIELA =   "GABRIELA";
	public static final String VOXCRED =   "VOXCRED";
	public static final String PERNAMBUCANAS =   "PERNAMBUCANAS";
	public static final String PAGSEGURO =   "PAGSEGURO";
	public static final String CASELI =   "CASELI";
}
