package br.com.conductor.wsfabrica.Reports;


import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import br.com.conductor.wsfabrica.common.Property;
import br.com.conductor.wsfabrica.util.Log;
import br.com.conductor.wsfabrica.util.Utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseReport {
	/*
	 * ============================================================================================================================
	 * Classe: BaseReport
	 * 
	 * Fun��o: Utilizada para fazer o gerenciamento do report HTML gerado atrav�s dos testes. 
	 * 
	 * Data de cria��o: 19/09/2016
	 * 
	 * Altera��es: 
	 * 19/09/2016 - Cria��o da classe. Cria��o dos m�todos iniciais de teste. 
	 * 
	 * Autor: Marcos Carvalho (marcos.carvalho@conductor.com.br)
	 * ============================================================================================================================
	 */
	
	
	
	private static ExtentReports extentReports; 
	private static String path;
	private static ExtentTest test;
	// Parte static propositalmente para facilitar a inicializa��o dos componentes essenciais e que devem
	// sobreviver ao longo de toda a aplica��o. 
	static {
		path = "src/test/resources/reports/"+Property.EMISSOR+"/"+BaseReport.retornarDataAtual()+"/report"+Property.EMISSOR+"-"+BaseReport.retornarDataAtual()+".html";
		Utils.tratarArquivoBaseReport(path);
		extentReports = new ExtentReports(path,false, new Locale("pt","BR"));
		File arquivo = Utils.retornarArquivoConfiguracao("src/test/resources/extent-config.xml");
		if(arquivo == null) Log.info("N�o foi poss�vel ler o arquivo de configura��o do Extent");
		else extentReports.loadConfig(arquivo);
		extentReports.assignProject("wscdtexterno");
		BaseReport.configMongoConnection(Property.IP_DB_MONGO,Property.PORT_DB_MONGO);
		
		
	}
	
	
	public static void iniciarTeste(String nomeTeste){
		test = extentReports.startTest(nomeTeste);
		test.log(LogStatus.INFO, "[Iniciando o Teste: "+nomeTeste+"]");
		Log.info("[Iniciando o Teste: "+nomeTeste+"]");
	}
	
	public static void iniciarTeste(String nomeTeste, String descricao){
		test = extentReports.startTest(nomeTeste,descricao);
	}
	
	public static void logInfo(String mensagem){
		if(test != null){
			test.log(LogStatus.INFO, mensagem);
			Log.info(mensagem);
		} else {
			Log.info("N�o foi poss�vel logar o teste no report. Verifique e tente novamente");
		}
	}
	
	public static void logInfo(String mensagem, String requisicao){
		if(test != null){
			test.log(LogStatus.INFO, mensagem);
			test.log(LogStatus.INFO, requisicao);
			Log.info(mensagem);
			Log.info(requisicao);
		} else {
			Log.info("N�o foi poss�vel logar o teste no report. Verifique e tente novamente");
		}
	}
	
	/*
	 * M�todo: finalizarTesteComSucesso.
	 * 
	 * Fun��o: Fazer a finaliza��o do teste com mensagem de sucesso. 
	 * */
	public static void finalizarTesteComSucesso(String message, String nomeTeste){
		
		test.log(LogStatus.PASS, message);
		test.log(LogStatus.INFO, "[Finalizando o Teste: "+nomeTeste+"]");
		Log.info(message);
		Log.info("[Finalizando o Teste: "+nomeTeste+"]");
		extentReports.endTest(test);
		extentReports.flush();
	}
	/*
	 * M�todo: finalizarTesteSemSucesso.
	 * 
	 * Fun��o: Fazer a finaliza��o do teste com mensagem de erro. 
	 * */
	public static void finalizarTesteSemSucesso(String message){
		
		test.log(LogStatus.FAIL, message);
		Log.erro(message);
		extentReports.endTest(test);
		extentReports.flush();
		
	}
	/*
	 * M�todo: retornarDataAtual.
	 * 
	 * Fun��o: retorna a data em um certo formato para constru��o da pasta onde o report ser� gravado.  
	 * */
	public static String retornarDataAtual(){
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		Calendar calendar = Calendar.getInstance();
		return format.format(calendar.getTime());
	}
	/*
	 * M�todo: configMongoConnection
	 * 
	 * Fun��o: Configura a conex�o com o mongo se ele estiver habilitado na m�quina. Neste m�todo ele utiliza a porta e 
	 * IP Padr�o (Localhost,27017) 
	 * */
	public static boolean configMongoConnection(){
		
		try{
			String ip = "127.0.0.1";
			int port = 27017;
			Socket socket = new Socket(ip,port);
			if(socket.isConnected()){
				socket.close();
				extentReports.x();
				Log.info("Extent Reports conectou - se ao MongoDB corretamente.");
				return true;
			} else {
				// For�a o fechamento do socket de qualquer maneira. 
				socket.close();
				Log.info("Extent Reports n�o se conectou ao mongoDB. Fazendo o report sem grava��o de hist�rico");
			}
			
		}catch(IOException e){
			Log.info("Extent Reports n�o se conectou ao mongoDB. Fazendo o report sem grava��o de hist�rico");
		}
		return false; 
		
	}
	/*
	 * M�todo: configMongoConnection
	 * 
	 * Fun��o: Configura a conex�o com o mongo se ele estiver habilitado na m�quina. Neste m�todo ele utiliza a o ip configurado
	 * e porta padr�o (27017)
	 * */
	public static boolean configMongoConnection(String ip){
		boolean conected = false;
		try{
			int port= 27017; // default do mongo. 
			Socket socket = new Socket(ip,port);
			if(socket.isConnected()){
				socket.close();
				extentReports.x(ip);
				Log.info("Extent Reports conectou - se ao MongoDB corretamente.");
				return true;
			} else {
				// For�a o fechamento do socket de qualquer maneira. 
				socket.close();
				Log.info("Extent Reports n�o se conectou ao mongoDB no ip informado.");
				Log.info("Tentando IP e porta padr�es.");
				conected = BaseReport.configMongoConnection();
			}
		}catch(IOException e){
			Log.info("Extent Reports n�o se conectou ao mongoDB no ip informado.");
			Log.info("Tentando IP e porta padr�es.");
			conected = BaseReport.configMongoConnection();
		}
		return conected;
		
	}
	/*
	 * M�todo: configMongoConnection
	 * 
	 * Fun��o: Configura a conex�o com o mongo se ele estiver habilitado na m�quina. Neste m�todo ele utiliza a porta configurada
	 * e IP padr�o (Localhost ou 127.0.0.1)
	 * */
	public static boolean configMongoConnection(int port){
		boolean conected = false;
		try{
			String ip = "127.0.0.1";
			Socket socket = new Socket(ip,port);
			
			if(socket.isConnected()){
				socket.close();
				extentReports.x(ip);
				Log.info("Extent Reports conectou - se ao MongoDB corretamente.");
				return true;
			} else {
				// For�a o fechamento do socket de qualquer maneira. 
				socket.close();
				Log.info("Extent Reports n�o se conectou ao mongoDB na porta informada.");
				Log.info("Tentando IP e porta padr�es.");
				conected = BaseReport.configMongoConnection();
			}
		}catch(IOException e){
			Log.info("Extent Reports n�o se conectou ao mongoDB na porta informada.");
			Log.info("Tentando IP e porta padr�es.");
			conected = BaseReport.configMongoConnection();
		}
		return conected;
		
	}

	/*
	 * M�todo: configMongoConnection
	 * 
	 * Fun��o: Configura a conex�o com o mongo se ele estiver habilitado na m�quina. Neste m�todo ele utiliza IP e porta 
	 * configurados. Ele � o primeiro a ser chamado, uma vez que sempre � considerado que a porta e IP s�o configurados
	 * no properties, lido na classe Properties (config.properties ou no config do emissor, pois ai h� uma maior flexibilidade).
	 * */
	public static boolean configMongoConnection(String ip, String port){
		Log.info("Iniciando conex�o com o MongoDB (IP: "+ip+" Porta: "+port+")");
		boolean conected = false;
		try{
			if((ip != null && !ip.isEmpty()) && (port!= null && !port.isEmpty())) {
				int portInteger = Integer.parseInt(port);
				Socket socket = new Socket(ip, portInteger);
				if (socket.isConnected()) {
					socket.close();
					extentReports.x(ip, portInteger);
					Log.info("Extent Reports conectou - se ao MongoDB corretamente.");
					return true;
				} else {
					// For�a o fechamento do socket de qualquer maneira.
					socket.close();
					Log.info("Extent Reports n�o se conectou ao mongoDB no ip/porta informados.");
					Log.info("Tentando IP e porta padr�es.");
					conected = BaseReport.configMongoConnection();
				}
			}else if((ip == null || ip.isEmpty()) &&  (port != null && !port.isEmpty())){
				Log.info("IP n�o configurado. Usando IP padr�o (Localhost) e porta configurada");
				int convertedPortInteger = Integer.parseInt(port);
				conected = BaseReport.configMongoConnection(convertedPortInteger);
			}
			else if((ip != null && !ip.isEmpty()) &&(port == null || port.isEmpty())){
				Log.info("Porta n�o configurada. Usando IP configurado e porta padr�o.");
				conected = BaseReport.configMongoConnection(ip);
			} else{
				Log.info("Nada configurado. Usando valores padr�es.");
				conected= BaseReport.configMongoConnection();
			}
			
		}catch(IOException e){
				Log.info("Extent Reports n�o se conectou ao mongoDB. Tentando IP/Porta Padr�es");
				conected= BaseReport.configMongoConnection();
		}catch(NumberFormatException e){
			if(ip != null && !ip.isEmpty()){
				Log.info("Erro ao tentar conectar o Extent Reports na porta configurada. tentando usar IP configurado e porta padr�o");
				BaseReport.configMongoConnection(ip);
			} else{
				Log.info("Erro ao tentar conectar o Extent Reports no IP/porta configurada. tentando usar IP/Porta padr�es");
				BaseReport.configMongoConnection();
			}
			
		}
		return conected; 
	}
}
