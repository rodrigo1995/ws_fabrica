package br.com.conductor.wsfabrica.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Assume;

import br.com.conductor.annotations.testannotations.IgnoreCaseTestClassFor;
import br.com.conductor.annotations.testannotations.IgnoreTestFor;
import br.com.conductor.annotations.testannotations.OnlyTestClassFor;
import br.com.conductor.annotations.testannotations.OnlyTestMethodFor;
import br.com.conductor.wsfabrica.Reports.BaseReport;
import br.com.conductor.wsfabrica.common.Property;



/**
 * Classe com m�todos de apoio, que otimizam a codifica��o das classes de
 * p�gina.
 * 
 * @author thiago.freire
 * 
 */
public abstract class Utils {
	private static String OS = System.getProperty("os.name").toLowerCase();
	
	public static String extrairStackTrace(Exception e) {
		StackTraceElement[] stack = e.getStackTrace();
		String exception = "";
		for (StackTraceElement s : stack) {
			exception = exception + s.toString() + "\n\t\t";
		}
		return exception;
	}

	public static String geraCPF() {
		GeradorCpf geradorCpf = new GeradorCpf();
		String iniciais = "";
		Integer numero;
		for (int i = 0; i < 9; i++) {
			numero = new Integer((int) (Math.random() * 10));
			iniciais += numero.toString();
		}
		return iniciais + geradorCpf.calcDigVerif(iniciais);
	}
	
	public static String conversorStringDoubleString(String valor){
		double d = Double.parseDouble(valor);
		DecimalFormat df=new DecimalFormat("0.00");  
		String novoValor = df.format(d);
		return novoValor;
	} 
	
	public static String formataCPF(String cpf) {
		String bloco1 = cpf.substring(0, 3);
		String bloco2 = cpf.substring(3, 6);
		String bloco3 = cpf.substring(6, 9);
		String bloco4 = cpf.substring(9, 11);
		cpf = bloco1+"."+bloco2+"."+bloco3+"-"+bloco4;

		return cpf;
	}
	
	public static String formataDataTime(String dataTime){
		String ano = dataTime.substring(0,4);
		String mes = dataTime.substring(5,7);
		String dia = dataTime.substring(8,10);
		String hh = dataTime.substring(11,13);
		String mm = dataTime.substring(14,16);
		String ss = dataTime.substring(17,19);
		return dia+"/"+mes+"/"+ano+" "+hh+":"+mm+":"+ss;
	}
	
	public static String formataData(Date data) {
		SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
		return dt.format(data); 
	}
	
	public static Date removerAcionarDiasData(int numeroDiasParaSubtrair){
		Date dataFinal = new Date();
        Calendar calendarData = Calendar.getInstance();
        calendarData.setTime(dataFinal);
    
        //retorna data de in�cio
        calendarData.add(Calendar.DAY_OF_MONTH, numeroDiasParaSubtrair);
        Date dataInicial = calendarData.getTime();
        
        return dataInicial;
	}
	
	public static void printResposta(Object object) throws IllegalArgumentException, IllegalAccessException{
		 Field[] fields = object.getClass().getDeclaredFields();   
		   BaseReport.logInfo("*************************  RESPOSTA WEBSERVICE  *****************************");
	        for (Field classField : fields)
	        {
	        	classField.setAccessible(true);
	        	if(classField.getName().equalsIgnoreCase("codRetorno") || classField.getName().equalsIgnoreCase("descricaoRetorno") || classField.getName().equalsIgnoreCase("idLog")){
	        		BaseReport.logInfo("["+classField.getName()+": "+classField.get(object)+"]");
	        	}
	        }
	        BaseReport.logInfo("*****************************************************************************");
	}
	
	public static void printRespostaAcordoIncluido(Object object) throws IllegalArgumentException, IllegalAccessException{
		 Field[] fields = object.getClass().getDeclaredFields();   
		   BaseReport.logInfo("*************************  RESPOSTA WEBSERVICE  *****************************");
	        for (Field classField : fields)
	        {
	        	classField.setAccessible(true);
	        	if(classField.getName().equalsIgnoreCase("codRetorno") || classField.getName().equalsIgnoreCase("mensagem") || classField.getName().equalsIgnoreCase("idLog")){
	        		BaseReport.logInfo("["+classField.getName()+": "+classField.get(object)+"]");
	        	}
	        }
	        BaseReport.logInfo("*****************************************************************************");
	}

	public static void tratarArquivoBaseReport(String pathDoc){
		try{
			String realPath = new File(pathDoc).getCanonicalPath();
			File file = new File(realPath);
			File diretorio;
			if(Utils.isWin()) {
				diretorio = new File(realPath.substring(0,realPath.lastIndexOf("\\")).concat("\\"));
			}else {
				diretorio = new File(realPath.substring(0,realPath.lastIndexOf("/")).concat("/"));
			}
			if(!file.canRead()){
				if((!diretorio.isDirectory()) && diretorio.mkdirs());
					//O arquivo era criado aqui, no entanto estava dando problema com o Extents. Deixei apenas para ele criar
					// as pastas.
				else {
					Log.info("Diret�rio de report n�o pode ser criado ou j� foi criado.");
				}
			}
		}catch(IOException e){
			e.printStackTrace();
			Log.info("Arquivo de report n�o p�de ser criado");
		}
		
	}

	public static void wait(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static File retornarArquivoConfiguracao(String caminhoArquivo) {
		File arquivo = new File(caminhoArquivo);
		String diretorioReal = arquivo.getAbsolutePath();
		File arquivoReal = new File(diretorioReal);
		if(arquivoReal.isFile()){
			return arquivoReal;
		}
		return null;
		
		
	}
	
	public static boolean isWin(){
		if(OS.contains("win")) return true;
		return false;
	}
	
	public static boolean isLinux(){
		if(OS.contains("linux")) return true;
		return false;
	}
	
	public static String comporMensagemFalha(String campo, String esperado, String retornado){
		return "Valida��o do "+campo+" incorreto(a). Valor Esperado: "+esperado+" - Valor Retornado: "+retornado;
	}
	
	public static String comporMensagemFalha(String campo, int esperado, int retornado){
		return "Valida��o do(a) "+campo+" incorreto(a). Valor Esperado: "+esperado+" - Valor Retornado: "+retornado;
	}
	
	public static String comporMensagemNulaFalha(String campo, String retornado){
		return "Valida��o do "+campo+" incorreto(a). Valor Esperado: null - Valor Retornado: "+retornado;
	}
	
	public static void validarErroSistema( Object resp,int codRetorno, String descricaoRetorno, boolean isFinishReport,String nomeTeste){
		String mensagemErro = new String();
		String descricaoRetornoGetRespObject = null;
		String codigoRetornoGetRespObject = null;
		try{
		Class<?> clazz = resp.getClass();
		for(Method metodo : clazz.getMethods() ){
			if(metodo.getName().equalsIgnoreCase("getResp")){
				Object getRespObject = metodo.invoke(resp);
				Class<?> classGetRespObject = getRespObject.getClass();
				for(Method metodoGetRespObject : classGetRespObject.getMethods() ){
					if(metodoGetRespObject.getName().equalsIgnoreCase("getDescricaoRetorno") && descricaoRetorno != null){
						if(metodoGetRespObject.invoke(getRespObject) == null){
							BaseReport.finalizarTesteSemSucesso("M�todo retornou um valor nulo para a descri��o do retorno. Esperado: "+descricaoRetorno);
							throw new IllegalArgumentException("M�todo retornou um valor nulo para a descri��o do retorno. Esperado: "+descricaoRetorno);
						}else {
							mensagemErro = "Valida��o do Descri��o Retorno incorreto. Valor Esperado: "+descricaoRetorno+" - Valor Retornado: "+descricaoRetornoGetRespObject;
							descricaoRetornoGetRespObject = metodoGetRespObject.invoke(getRespObject).toString();
							Assert.assertEquals(descricaoRetornoGetRespObject, descricaoRetorno);
						}
					} else if(metodoGetRespObject.getName().equalsIgnoreCase("getDescricaoRetorno")){
						mensagemErro = "Descri��o do retorno deveria retornar nulo.";
						Assert.assertNull(mensagemErro,descricaoRetorno);
					} else if(metodoGetRespObject.getName().equalsIgnoreCase("getCodRetorno")){
						if(metodoGetRespObject.invoke(getRespObject) == null){
							BaseReport.finalizarTesteSemSucesso("M�todo retornou um valor nulo para o c�digo de retorno. Esperado: "+codRetorno);
							throw new IllegalArgumentException("M�todo retornou um valor nulo para o c�digo de retorno. Esperado: "+codRetorno);
						}else {
							codigoRetornoGetRespObject = (metodoGetRespObject.invoke(getRespObject).toString());
							mensagemErro = Utils.comporMensagemFalha("C�digo de retorno",String.valueOf(codRetorno), codigoRetornoGetRespObject);
							Assert.assertEquals(mensagemErro,String.valueOf(codRetorno),codigoRetornoGetRespObject);	
						}
					}else if(metodoGetRespObject.getName().equalsIgnoreCase("getIDLog")){
						mensagemErro = "ID do log deveria retornar nulo";
						Assert.assertNull(mensagemErro,metodoGetRespObject.invoke(getRespObject));
					}
				}
			}
		}
		
		if(isFinishReport) BaseReport.finalizarTesteComSucesso("Sucesso na valida��o do retorno deste m�todo", nomeTeste);
		else BaseReport.logInfo("Sucesso na valida��o do retorno deste m�todo");
		
		}catch(InvocationTargetException e){
			BaseReport.finalizarTesteSemSucesso("Erro ao retornar as informa��es para valida��o do retorno do Webservice.");
			Log.excecao(e);
		}catch(IllegalAccessException e){
			BaseReport.finalizarTesteSemSucesso("Erro ao retornar as informa��es para valida��o do retorno do Webservice.");
			Log.excecao(e);
		}catch(AssertionError e){
			if(mensagemErro.isEmpty()){
				BaseReport.finalizarTesteSemSucesso("Ocorreu um erro na valida��o do retorno do m�todo. Verifique as entradas e tente novamente");
				throw new IllegalArgumentException("Ocorreu um erro na valida��o do retorno do m�todo. Verifique as entradas e tente novamente");
			}
			else {
				BaseReport.finalizarTesteSemSucesso(mensagemErro);
				throw new IllegalArgumentException(mensagemErro);
			}
		}
	}
	
	public static void validarErroSistema(int codRetornoResp, String descricaoRetornoResp,int codRetornoEsperado, String descricaoRetornoEsperado, boolean isFinishReport,String nomeTeste){
		String mensagemErro = new String();
		try{
			if(descricaoRetornoResp != null) {
				mensagemErro = "Validacao do Descri��o Retorno incorreto. Valor Esperado: "+descricaoRetornoEsperado+" - Valor Retornado: "+descricaoRetornoResp;
				Assert.assertEquals(descricaoRetornoResp, descricaoRetornoEsperado);
			} else if (descricaoRetornoResp == null && descricaoRetornoEsperado == null) {
				mensagemErro = "Valida��o do Descri��o Retorno incorreto. Valor Esperado: "+descricaoRetornoEsperado+" - Valor Retornado: "+descricaoRetornoResp;
				Assert.assertEquals(descricaoRetornoResp, descricaoRetornoEsperado);
			} else {
				BaseReport.finalizarTesteSemSucesso("M�todo retornou um valor nulo para a descri��o do retorno. Esperado: "+descricaoRetornoResp);
				throw new IllegalArgumentException("M�todo retornou um valor nulo para a descri��o do retorno. Esperado: "+descricaoRetornoResp);
			}
			
			if(String.valueOf(codRetornoResp) != null){
				mensagemErro = Utils.comporMensagemFalha("C�digo de retorno", String.valueOf(codRetornoResp), String.valueOf(codRetornoEsperado));
				Assert.assertEquals(mensagemErro,String.valueOf(codRetornoEsperado),String.valueOf(codRetornoResp));	
			} else {
				BaseReport.finalizarTesteSemSucesso("Metodo retornou um valor nulo para o codigo de retorno. Esperado: "+codRetornoEsperado);
				throw new IllegalArgumentException("Metodo retornou um valor nulo para o codigo de retorno. Esperado: "+codRetornoEsperado);
			}
					
		if(isFinishReport) BaseReport.finalizarTesteComSucesso("Sucesso na validacao do retorno deste metodo", nomeTeste);
		else BaseReport.logInfo("Sucesso na validacao do retorno deste metodo");
		
		}catch(AssertionError e){
			if(mensagemErro.isEmpty()){
				BaseReport.finalizarTesteSemSucesso("Ocorreu um erro na valida��o do retorno do m�todo. Verifique as entradas e tente novamente");
				throw new IllegalArgumentException("Ocorreu um erro na valida��o do retorno do m�todo. Verifique as entradas e tente novamente");
			}
			else {
				BaseReport.finalizarTesteSemSucesso(mensagemErro);
				throw new IllegalArgumentException(mensagemErro);
			}
		}
	}
	
	public static void verificaTesteEmissor(String methodName, Class<?> clazz) {
		for(Method method: clazz.getDeclaredMethods()){
			boolean isPresent = false;
			if(method.getName().equalsIgnoreCase(methodName)){
				if(method.isAnnotationPresent(IgnoreTestFor.class)){
					IgnoreTestFor ignorar = method.getAnnotation(IgnoreTestFor.class);
					String[] emissoresIgnorados = ignorar.emissores();
					for(int a = 0; a < emissoresIgnorados.length; a++){
						if(emissoresIgnorados[a].equalsIgnoreCase(Property.EMISSOR)){
							isPresent = true;
						}
					}
					if(isPresent){
						Log.info("Teste: "+methodName+" ignorado para o emissor: "+Property.EMISSOR);
						Assume.assumeTrue("Teste: "+methodName+" ignorado para o emissor: "+Property.EMISSOR,false);
					}
				} else if(method.isAnnotationPresent(OnlyTestMethodFor.class)){
					isPresent = false;
					OnlyTestMethodFor onlyTestMethodFor =  method.getAnnotation(OnlyTestMethodFor.class);
					String[] emissoresExecutados = onlyTestMethodFor.emissores();
					for(String emissor:emissoresExecutados){
						if(emissor.equalsIgnoreCase(Property.EMISSOR)){
							isPresent = true;
						}
					}
					if(!isPresent){
						Log.info("Teste: "+methodName+" ignorado para o emissor: "+Property.EMISSOR);
						Assume.assumeTrue("Teste: "+methodName+" ignorado para o emissor: "+Property.EMISSOR,false);
					}
				}
			}
		}
		
		
	}
	
public static void verificaTesteClasseEmissor(Class<?> classExclusion){
		
		if(classExclusion.isAnnotationPresent(OnlyTestClassFor.class)){
			boolean isPresent = false;
			OnlyTestClassFor testFor = classExclusion.getAnnotation(OnlyTestClassFor.class);
			String[] emissoresExecutados = testFor.emissores();
			for(String emissor: emissoresExecutados){
				if(emissor.equalsIgnoreCase(Property.EMISSOR)){
					isPresent = true;
				}
			}
			if(!isPresent){
				Log.info("Classe de teste: "+classExclusion.getSimpleName()+" ignorada para: "+Property.EMISSOR);
				Assume.assumeTrue("Classe de teste: "+classExclusion.getSimpleName()+" ignorada para: "+Property.EMISSOR,false);
			}
		} 
		if(classExclusion.isAnnotationPresent(IgnoreCaseTestClassFor.class)){
			boolean isPresent = false;
			IgnoreCaseTestClassFor ignoreFor = classExclusion.getAnnotation(IgnoreCaseTestClassFor.class);
			String[] emissoresIgnorados = ignoreFor.emissores();
			for(String emissor: emissoresIgnorados){
				if(emissor.equalsIgnoreCase(Property.EMISSOR)){
					isPresent = true;
				}
			}
			if(isPresent){
				Log.info("Classe de teste: "+classExclusion.getSimpleName()+" ignorada para: "+Property.EMISSOR);
				Assume.assumeTrue("Classe de teste: "+classExclusion.getSimpleName()+" ignorada para: "+Property.EMISSOR,false);
			}
		}
		
	}

	public static void verificaEmissorAcessaMenu(String methodName,
			Class<?> classAnnotation) {
		verificaTesteEmissor(methodName, classAnnotation);
		verificaTesteClasseEmissor(classAnnotation);

	}
	
	public static void assertThat(String actual, Matcher<String> matcher) {
		Assert.assertThat(actual, matcher);
	}
	
}