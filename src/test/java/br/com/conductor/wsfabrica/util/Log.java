package br.com.conductor.wsfabrica.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;

import br.com.conductor.wsfabrica.common.Property;


public class Log {

	private static final Logger aLogger = LogUtils.getLogger();
	private static final SimpleDateFormat aFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	
	private static void mensagem(String pMensagemFinal) {
		aLogger.info(pMensagemFinal);
	}
	private static void mensagemDebug(String pMensagemFinal) {
		aLogger.debug(pMensagemFinal);
	}


	public static void erro(String pMensagem) {
		try {
			String vMensagemFinal = getCabecalho("ERRO") + pMensagem;
			mensagem(vMensagemFinal);
		} catch (Exception e) {
			// Se der erro aqui eu nao posso fazer mais eh nada.
		}
	}
	public static void debug(String pMensagem) {
		try {
			String vMensagemFinal = getCabecalho("DEBUG") + pMensagem;
			mensagemDebug(vMensagemFinal);
		} catch (Exception e) {
			// Se der erro aqui eu nao posso fazer mais eh nada.
		}
	}
	public static void info(String pMensagem) {
		try {
			mensagem(getCabecalho("INFO") + pMensagem);
		} catch (Exception e) {
			// Se der erro aki eu nao posso fazer mais eh nada.
		}
	}
	private static String getCabecalho(String pTipo) {
		return String.format("[%s] [%s] [%s] ", pTipo, aFormat.format(Calendar.getInstance().getTime()), Property.EMISSOR);
	}
	
	public static void excecao(Exception vExcecao) {
		if (vExcecao != null) {
			StringBuilder vBuilder = new StringBuilder();
		
			vBuilder.append("[EXCE��O OCORRIDA]: " + vExcecao.getMessage());
			vBuilder.append('\n');
			vBuilder.append('\n');
			vBuilder.append('\n' + Utils.extrairStackTrace(vExcecao));
			vBuilder.append('\n');
			erro(vBuilder.toString());
		}
	}
}
