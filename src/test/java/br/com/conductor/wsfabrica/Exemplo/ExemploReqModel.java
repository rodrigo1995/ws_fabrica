package br.com.conductor.wsfabrica.Exemplo;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map.Entry;

import br.com.conductor.wsfabrica.dao.DBUtils;
import br.com.conductor.wsfabrica.dao.ResultadoConsulta;
import br.com.conductor.wsfabrica.ws_exemplo.ExemploReq;

public class ExemploReqModel {
	String param1;
	String param2;
	int param3;
	
	public ExemploReqModel() {
	}
	
	public ExemploReqModel(String param1, String param2, int param3) {
		this.param1 = param1;
		this.param2 = param2;
		this.param3 = param3;
	}
	
	 public String getParam1() {
	    	return param1;
	    }
	    
	    public void setParam1(String value) {
	    	this.param1 = value;
	    }
	    
	    public String getParam2() {
	    	return param2;
	    }
	    
	    public void setParam2(String value) {
	    	this.param2 = value;
	    }
	    
	    public int getParam3() {
	    	return param3;
	    }
	    
	    public void setParam3(int value) {
	    	this.param3 = value;
	    }
	    
	    
	    public ExemploReqModel getMassaDados(String query) throws IllegalArgumentException, IllegalAccessException {
	    	//Essa classe acessa o banco, realiza a consulta passada pelo parametro query e retorna os parametros necessarios
	    	//para requisicao
	    	DBUtils dbUtils = new DBUtils();
			ResultadoConsulta resultadoConsulta = dbUtils.getConsulta(query);
			while(resultadoConsulta == null) {
				resultadoConsulta = dbUtils.getConsulta(query);
			}
			HashMap<String, String> informacaoBanco = null ; 
			
			informacaoBanco = resultadoConsulta.retornarRegistros(resultadoConsulta);
				
			ExemploReqModel exemploReqModel = new ExemploReqModel();
			Field[] fieldInfAcordo = exemploReqModel.getClass().getDeclaredFields();
			for (Entry<String, String>  informacao : informacaoBanco.entrySet()) {
				for (Field classField : fieldInfAcordo)
				{
					classField.setAccessible(true);
					if(informacao.getKey().equalsIgnoreCase(classField.getName())){
						classField.set(this, informacao.getValue());
					}
				}
			}
			
			exemploReqModel = new ExemploReqModel(this.getParam1(),this.getParam2(), this.getParam3());
			
			return exemploReqModel;
	    }
	
	    public void validarExemplo(ExemploReq req,String statusReqisicao) {
	    	/*EXECUTA A ROTINA DE VALIDACAO DOS DADOS DA REQUISICAO E
	    	 * DO STATUS RETORNADO PELA RESPONSE DA REQUISICAO
	    	 */
	    	
	    }
	
}
